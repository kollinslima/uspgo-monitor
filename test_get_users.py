import json
import requests

url = "http://localhost:5000/location/get"
data = {"username":"123","latitude":"20", "longitude":"10", "radius":"1"}
headers = {'Content-type': 'application/json', 'Authorization': 'Basic username:password'}

response= requests.post(url, data=json.dumps(data), headers=headers)
json_data = json.loads(response.content)

'''
print(json_data)
print(json_data['users'])
'''

for user in json_data['users']:
    print('------------------')
    print(user['username'])
    print(user['latitude'])
    print(user['longitude'])

